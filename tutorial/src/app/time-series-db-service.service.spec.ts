import { TestBed } from '@angular/core/testing';

import { TimeSeriesDbServiceService } from './time-series-db-service.service';

describe('TimeSeriesDbServiceService', () => {
  let service: TimeSeriesDbServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TimeSeriesDbServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
